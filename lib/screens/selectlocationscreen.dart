import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:tansport_service_api/Model/route_location.dart';
import 'package:tansport_service_api/screens/confirm_pickup.dart';
import 'package:tansport_service_api/screens/pin_mapscreen.dart';
import 'package:tansport_service_api/secrets.dart';
import 'package:uuid/uuid.dart';
import 'package:geocoder/geocoder.dart';

import '../address_search.dart';
import '../place_service.dart';

const kGoogleApiKey = "AIzaSyBH6v3Nd0wJh0d5ybcWbpJnXy0CRD-7Xis";

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: Secrets.API_KEY);

class SelectLocationScreen extends StatefulWidget {
  double userLat, userlong;

  SelectLocationScreen({Key key, this.userLat, this.userlong}) : super(key: key);

  @override
  _SelectLocationScreenState createState() => _SelectLocationScreenState();
}

class _SelectLocationScreenState extends State<SelectLocationScreen> {

  final _dropOffLocation_textcontroller = TextEditingController();
  String _streetNumber = '';
  String _street = '';
  String _city = '';
  String _zipCode = '';
  String _address;

  RouteLocation userRoute = new RouteLocation();

  @override
  void dispose() {
    // TODO: implement dispose
    _dropOffLocation_textcontroller.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: SafeArea(
            child: Column(
              children: [
                Container(
                  color: Colors.white,
//                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.only(top: 15, left: 25, right: 25),
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                                icon: Icon(Icons.arrow_back_ios, size: 22, color: Colors.grey,),
                              onPressed: (){
                                  Navigator.pop(context);
                              },
                              ),
//                          _arrow(),
//                          _pinIcon()
                            IconButton(
                              icon: Icon(Icons.map, size: 25, color: Colors.blue.shade200,),
                              onPressed: (){
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) => (PinMapScreen(currentLat: widget.userLat, currentLong: widget.userlong,))));
                              },
                            ),
                          ],
                        ),
                      ),
                      _input()
                    ],
                  ),
                ),
                Container(
                  color: Color(0xFFf5fafc),
                  padding: EdgeInsets.only(top: 30, bottom: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      _addLocation('ADD HOME'),
                      _addLocation('ADD WORK'),
                      _addLocation('ADD PLACE'),
                    ],
                  ),
                ),
                _savedLocation(),
                Container(
                  height: 10,
                  color: Color(0xFFf5fafc),
                ),
                _lastVisited(),
                _button()
              ],
            ),
        ),
      ),
    );
  }

  Widget _arrow() {
    return Container(
      width: 40,
      height: 40,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5),
            topRight: Radius.circular(5),
            bottomLeft: Radius.circular(5),
            bottomRight: Radius.circular(5)
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 7,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
      ),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(left: 5),
          child: IconButton(
            icon: Icon(Icons.arrow_back_ios, size: 25,),
            color: Colors.grey,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
        ),
      ),
    );
  }

  Widget _pinIcon(){
    return GestureDetector(
      onDoubleTap: (){
        print('Clicked');
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => (PinMapScreen(currentLat: widget.userLat, currentLong: widget.userlong,))));
      },
      child: Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5)
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 7,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: Center(
          child: IconButton(
            icon: Icon(Icons.map),
            color: Colors.blue.shade200,
            onPressed: (){},
          ),
        ),
      ),
    );
  }

  Widget _inputLocation(){
    return Container(
      padding: EdgeInsets.only(top: 20, bottom: 10),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.location_on,
                color: Colors.redAccent,
                size: 26,
              ),
              SizedBox(width: 20,),
              Column(
                children: [
                  Container(
                    width:  MediaQuery.of(context).size.width - 144,
                    height: 30,
                    child: TextFormField(
                      readOnly: true,
//                          controller: _textController,
//                      onTap: () async {
//                        Prediction p = await PlacesAutocomplete.show(
//                        context: context, apiKey: kGoogleApiKey);
//                        displayPrediction(p);
//                        },
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Current Location',
                        hintStyle: TextStyle(
                          color: Colors.grey,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],

          ),
          const Divider(
            color: Colors.grey,
            indent: 45,
            thickness: 0.5,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 16,
                height: 16,
                decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('images/gps.png'),
                      fit: BoxFit.fill,
                    )
                ),
              ),
//                Icon(
//                  Icons.pin_drop,
//                  color: Colors.deepOrangeAccent,
//                  size: 25,
//                ),
              SizedBox(width: 30,),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 144,
                      // color: Colors.grey,
                      height: 30,
                      child: TextField(
                        controller: _dropOffLocation_textcontroller,
                        
                        readOnly: true,
                          onTap: () async {
                            Prediction p = await PlacesAutocomplete.show(
                                context: context, apiKey: kGoogleApiKey);
                            displayPrediction(p);
//                            final sessionToken = Uuid().v4();
//                            final Suggestion result = await showSearch(
//                              context: context,
//                              delegate: AddressSearch(sessionToken),
//                            );
//                            // This will change the text displayed in the TextField
//                            if (result != null) {
//                            final placeDetails = await PlaceApiProvider(sessionToken)
//                                .getPlaceDetailFromId(result.placeId);
//                              setState(() {
//                                _dropOffLocation_textcontroller.text = result.description;
//                                _streetNumber = placeDetails.streetNumber;
//                                _street = placeDetails.street;
//                                _city = placeDetails.city;
//                                _zipCode = placeDetails.zipCode;
//                                });
//                              }
                          },
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Destination',
                            hintStyle: TextStyle(
                              color: Colors.grey,
                              fontSize: 16,
                              fontWeight: FontWeight.normal,
                            )
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _addLocation (String typeName){
    return Container(
      child: Center(
        child: Column(
          children: [
            MaterialButton(
              onPressed: () {},
              shape: CircleBorder(
                side: BorderSide(
                  color: Colors.blue,
                  width: 1.0,
                  style: BorderStyle.solid
                )
              ),
              child: typeName == 'ADD HOME' ? Icon(Icons.home, color: Colors.blue,) : typeName == 'ADD WORK' ?
//              child: typeName == 'ADD HOME' ? Icon(Icons.home_outlined, color: Colors.blue,) : typeName == 'ADD WORK' ?
              Icon(Icons.work, color: Colors.blue,) : Icon(Icons.add_a_photo, color: Colors.blue,),
//              Icon(Icons.work_outline_outlined, color: Colors.blue,) : Icon(Icons.add_location_alt_outlined, color: Colors.blue,),
            ),
            Text(typeName,
              style: TextStyle(
                color: Colors.blue
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _savedLocation(){
    return Container(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Icon(Icons.bookmark_border, size: 30,),
                SizedBox(width: 10,),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Saved Location',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),),
                      Text('Get your saved place here',
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.grey.shade500
                      ),)
                    ],
                  ),
                ),
              ],
            ),

            Icon(Icons.arrow_forward_ios)
          ],
        ),
      ),
    );
  }

  Widget _input(){
    return Container(
      padding: EdgeInsets.only(top: 20, bottom: 10),
//      width: MediaQuery.of(context).size.width - 20,
//      height: 110,
//      padding: EdgeInsets.all(10),
//      decoration: BoxDecoration(
//          borderRadius: BorderRadius.circular(10),
//          color: Colors.white
//      ),

      child: Column(
        children: [
          Container(
//            decoration: BoxDecoration(
//                borderRadius: BorderRadius.all(Radius.circular(5.0)),
//                border: Border.all(width: 0.5)
//            ),
            width: MediaQuery.of(context).size.width - 40,
            height: 30,
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(Icons.location_on, size: 23, color: Colors.red,),
                SizedBox(width: 10,),
                Container(
                  width: MediaQuery.of(context).size.width - 140,
                  height: 30,
                  child: TextField(
                    readOnly: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Pick up : Current Location',
                      hintStyle: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w200,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                Icon(Icons.close, size: 20,color: Colors.black54,),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Container(
                  width: 3,
                  height: 30,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('images/vertical_line.png'),
                        fit: BoxFit.fill
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25),
                child: SizedBox(
                  height: 30,
                  width: MediaQuery.of(context).size.width - 120,
                  child: Divider(
                    color: Colors.grey,
                    thickness: 0.5,
                  ),
                ),
              )
            ],
          ),
          Container(
//            decoration: BoxDecoration(
//                borderRadius: BorderRadius.all(Radius.circular(5.0)),
//                border: Border.all(width: 0.5)
//            ),
            width: MediaQuery.of(context).size.width - 40,
            // height: 30,
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 16,
                  height: 16,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('images/gps.png'),
                        fit: BoxFit.fill,
                      )
                  ),
                ),
                SizedBox(width: 10,),
                Container(
                  width: MediaQuery.of(context).size.width - 140,
                  height: 50,
                  // color: Colors.green,
//                  decoration: BoxDecoration(
//                    border: Border.all(
//                      width: 1,
//                    )
//                  ),
                  child: TextFormField(
                    maxLines: 3,
                    style: TextStyle(
                      color: Colors.black,
                        fontWeight: FontWeight.w200,
                        fontSize: 12,
                    ),
//                    readOnly: true,
//                    onTap: (){
//                      print('Clicked');
//                      Navigator.push(context, MaterialPageRoute(builder: (context) => (SelectLocationScreen(userLat: _currentPosition.latitude, userlong: _currentPosition.longitude,))));
//                    },
//                    readOnly: true,
                    controller: _dropOffLocation_textcontroller,
                    onTap: () async {
                      Prediction p = await PlacesAutocomplete.show(
                          context: context, apiKey: kGoogleApiKey,
                          language: "en",
                          components: [Component(Component.country, "kh")]);
                      displayPrediction(p);
                      PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
                      
            
                      userRoute.startLocation.lat = widget.userLat;
                      userRoute.startLocation.long = widget.userlong;
                      
                      print("----->== ${userRoute}");
                      // print ("----->${address}");
//                      _dropOffLocation_textcontroller.text = p.description.toString();
                      _dropOffLocation_textcontroller.text = "${_address}";

                      if (p == null){
                        showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return Dialog(
                              child: new Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new CircularProgressIndicator(),
                                  new Text("Loading"),
                                ],
                              ),
                            );
                          },
                        );
                        new Future.delayed(new Duration(seconds: 5), () {
                          Navigator.pop(context); //pop dialog
                        });
                      }
                      else {
                        print(detail);
                        // Navigator.push(context, MaterialPageRoute(builder: (context) => (ConfirmPickUp(address: address, userRoute: userRoute))));
                      }
                      // Navigator.push(context, MaterialPageRoute(builder: (context) => (ConfirmPickUp(address: address, userRoute: userRoute))));
                    },

                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Destination?',
                      hintStyle: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w200,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
                Icon(Icons.close, size: 20,color: Colors.black54,),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _lastVisited(){
    return Container(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Last Visited',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                fontFamily: 'Helvetica Neue'
              ),
            ),
            _location(),
            _location(),
            _location(),
          ],
        ),
      ),
    );
  }

  Widget _location(){
    return Container(
      child: Column(
        children: [
          const Divider(
            color: Colors.grey,
            indent: 0,
            thickness: 1,
          ),
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              children: [
                Icon(Icons.pin_drop, color: Colors.red,),
//                Icon(Icons.pin_drop_outlined, color: Colors.red,),
                SizedBox(width: 10,),
                Container(
                  width: MediaQuery.of(context).size.width - 104,
//                  color: Colors.grey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Institute of Technology of Cambodia',
//                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        fontFamily: 'Helvetica Neue'
                      ),),
                      Text('7.82km - Russian hvhf bfvgfv gsvgv vcvchfhj viusgudusyg yduyuyf diubhjdsbvhjdfbvhjdbf',
//                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w100,
                          fontFamily: 'Helvetica Neue',
                        color: Colors.grey.shade700
                      ),),
                    ],
                  ),
                ),
                Icon(Icons.bookmark_border, size: 30),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _button(){
    return Align(
            alignment: Alignment.bottomCenter,
            // child: Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 200,
            //   decoration: BoxDecoration(
            //     color: Colors.white,
            //     borderRadius: BorderRadius.only(
            //         topRight: Radius.circular(10.0),
            //         topLeft: Radius.circular(10.0),
            //     ),
            //   ),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: FlatButton(
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => (ConfirmPickUp(address: _address, userRoute: userRoute))));
                      },
                      child: Text('Search for driver',
                      style: TextStyle(
                        color: Colors.white
                      ),),
                      color: Colors.blue,
                      padding: EdgeInsets.only(top: 12, bottom: 12,
                          left:  140,
                          right:  140),
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0))
                    ),
                  ),
                ],
              ),
            // ),
          );
  }

  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
      await _places.getDetailsByPlaceId(p.placeId);

      var placeId = p.placeId;
      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;

      userRoute.endLocation.lat = detail.result.geometry.location.lat;
      userRoute.endLocation.long = detail.result.geometry.location.lng;

      var address = await Geocoder.local.findAddressesFromQuery(p.description);
      setState(() {
        _address = address.first.toString();
      });

      print(lat);
      print(lng);
    }
    else {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Dialog(
            child: new Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                new CircularProgressIndicator(),
                new Text("Loading"),
              ],
            ),
          );
        },
      );
      new Future.delayed(new Duration(seconds: 5), () {
        Navigator.pop(context); //pop dialog
      });
    }
  }

}
