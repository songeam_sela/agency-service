import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:marquee/marquee.dart';
import 'package:tansport_service_api/screens/selectlocationscreen.dart';
import 'package:tansport_service_api/secrets.dart';

class MapHomeScreen extends StatefulWidget {
  @override
  _MapHomeScreenState createState() => _MapHomeScreenState();
}

class _MapHomeScreenState extends State<MapHomeScreen> {
  GoogleMapController _mapController ;
  Completer<GoogleMapController> _controller = Completer();
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  Timer timer;
  String _address = "Current Location";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    timer = Timer.periodic(Duration(seconds: 5), (timer) {
      _getCurrentLocation();
//    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  _currentPosition == null ? Center(child: CircularProgressIndicator(),) : Stack(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: GoogleMap(
          mapType: MapType.normal,
          mapToolbarEnabled: false,
          scrollGesturesEnabled: true,
          zoomGesturesEnabled: true,
          zoomControlsEnabled: false,
          trafficEnabled: true,
          compassEnabled: true,
          indoorViewEnabled: true,
          rotateGesturesEnabled: true,
          tiltGesturesEnabled: true,
          myLocationButtonEnabled: false,
          myLocationEnabled: true,
          initialCameraPosition: CameraPosition(
            target: LatLng(
                _currentPosition.latitude, _currentPosition.longitude),
            zoom: 15,
          ),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
//            _modalBottomSheetMenu();
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 60),
          child: Align(
            alignment: Alignment.topCenter,
            child: _locationContainer(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 50, left: 20, right: 20),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: _destinationContainer(),
          ),
        )
//        _buttomSheet()
      ],
      ),
    );
  }


  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        // _getAddressFromLatLng(position);
        getAddress(position.latitude, position.longitude);
      });
    }).catchError((e) {
      print(e);
    });
  }


  Widget _locationContainer() {
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(Icons.location_on, size: 23, color: Colors.black,),
                  SizedBox(width: 10,),
                  SizedBox(
                    width: MediaQuery.of(context).size.width - 93,
                    child: Marquee(
                      text: _address,
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.normal,
                      ),
                      scrollAxis: Axis.horizontal,
                      pauseAfterRound: Duration(seconds: 2),
                      decelerationDuration: Duration(seconds: 2),
                      startPadding: 10.0,
                      blankSpace: 10.0,
                      numberOfRounds: 2,
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  Widget _destinationContainer(){
    return Container(
      width: MediaQuery.of(context).size.width - 40,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white
      ),
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 16,
            height: 16,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/gps.png'),
                fit: BoxFit.fill,
              )
            ),
          ),
          SizedBox(width: 30,),
          GestureDetector(
              child: Text('Destination?', style: TextStyle(
              color: Colors.blueGrey,
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
              ),
            ),
            onTap: (){
              print('Clicked');
                Navigator.push(context, MaterialPageRoute(builder: (context) => (SelectLocationScreen(userLat: _currentPosition.latitude, userlong: _currentPosition.longitude,))));
            },
          ),
        ],
      ),
    );
  }

  Future<String> getAddress(double l1, double l2) async {
    String url =
        "https://maps.googleapis.com/maps/api/geocode/json?&latlng=${l1},${l2}&&key=${Secrets.API_KEY}";
    http.Response response = await http.get(url);
    var values = jsonDecode(response.body);
    print("-----> values: $values");
    String address = values["results"][0]["formatted_address"];
    print("-----> address: $address");

    setState(() {
      _address = address;
      // placeId = values["results"][0]["place_id"];
    });
    return address;
  }


//   Widget _header(){
//     return Container(
//       width: MediaQuery.of(context).size.width,
//       height: 140,
//       decoration: BoxDecoration(
//         gradient: LinearGradient(
//           colors: [Colors.white, Colors.white10],
//           begin: Alignment.topCenter,
//           end: Alignment.bottomCenter,
//         ),
//       ),
//       child: Padding(
//         padding: const EdgeInsets.only(left: 10, right: 10),
//         child: Row(
// //          crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             Icon(Icons.menu, size: 30, color: Colors.black54,),
//             Text('Agency Service',
//             style: TextStyle(
//               fontSize: 20,
//               fontWeight: FontWeight.w500,
//               color: Colors.black54
//             ),),
//             Icon(Icons.notifications_none, size: 30, color: Colors.black54,),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget _bottomContainer(){
//     return Container(
//       width: MediaQuery.of(context).size.width - 20,
//       height: 110,
//       padding: EdgeInsets.all(10),
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(10),
//         color: Colors.white
//       ),

//       child: Column(
//         children: [
//           Container(
// //            decoration: BoxDecoration(
// //                borderRadius: BorderRadius.all(Radius.circular(5.0)),
// //                border: Border.all(width: 0.5)
// //            ),
//             width: MediaQuery.of(context).size.width - 40,
//             height: 30,
//             padding: EdgeInsets.only(left: 10, right: 10),
//             child: Row(
//               crossAxisAlignment: CrossAxisAlignment.center,
// //              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Icon(Icons.location_on, size: 23, color: Colors.red,),
//                 SizedBox(width: 10,),
//                 Container(
//                   width: MediaQuery.of(context).size.width - 140,
//                   height: 30,
//                   padding: EdgeInsets.only(left: 5),
//                   child: TextField(
//                     enabled: false,
//                     decoration: InputDecoration(
//                       border: InputBorder.none,
//                       hintText: 'Pick up : Current Location',
//                       hintStyle: TextStyle(
//                         color: Colors.black,
//                         fontWeight: FontWeight.w200,
//                         fontSize: 16,
//                       ),
//                     ),
//                   ),
//                 ),
// //                Icon(Icons.close, size: 20,color: Colors.black54,),
//               ],
//             ),
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(left: 20),
//                 child: Container(
//                   width: 3,
//                   height: 30,
//                   decoration: BoxDecoration(
//                     image: DecorationImage(
//                       image: AssetImage('images/vertical_line.png'),
//                       fit: BoxFit.fill
//                     ),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(left: 25),
//                 child: SizedBox(
//                   height: 30,
//                   width: MediaQuery.of(context).size.width - 120,
//                   child: Divider(
//                     color: Colors.grey,
//                     thickness: 0.5,
//                   ),
//                 ),
//               )
//             ],
//           ),
//           Container(
// //            decoration: BoxDecoration(
// //                borderRadius: BorderRadius.all(Radius.circular(5.0)),
// //                border: Border.all(width: 0.5)
// //            ),
//             width: MediaQuery.of(context).size.width - 40,
//             height: 30,
//             padding: EdgeInsets.only(left: 10, right: 10),
//             child: Row(
//               crossAxisAlignment: CrossAxisAlignment.start,
// //              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Container(
//                   width: 16,
//                   height: 16,
//                   decoration: BoxDecoration(
//                     image: DecorationImage(
//                       image: AssetImage('images/gps.png'),
//                       fit: BoxFit.fill,
//                     )
//                   ),
//                 ),
//                 SizedBox(width: 10,),
//                 Container(
//                   width: MediaQuery.of(context).size.width - 140,
//                   height: 30,
//                   padding: EdgeInsets.only(left: 12),
//                   child: TextField(
//                     readOnly: true,
//                     onTap: (){
//                       print('Clicked');
//                       Navigator.push(context, MaterialPageRoute(builder: (context) => (SelectLocationScreen(userLat: _currentPosition.latitude, userlong: _currentPosition.longitude,))));
//                     },
//                     decoration: InputDecoration(
//                       border: InputBorder.none,
//                       hintText: 'Destination?',
//                       hintStyle: TextStyle(
//                         color: Colors.blueGrey,
//                         fontWeight: FontWeight.w500,
//                         fontSize: 16,
//                       ),
//                     ),
//                   ),
//                 ),
// //                Icon(Icons.close, size: 20,color: Colors.black54,),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }


}
