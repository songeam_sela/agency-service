import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tansport_service_api/screens/pin_mapscreen.dart';
import 'package:tansport_service_api/secrets.dart';

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {



  GoogleMapController _mapController ;
  BitmapDescriptor _markerIcon;
  Completer<GoogleMapController> _controller = Completer();
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  Timer timer;
  bool loading = false;
  String _address = "";
  TextEditingController _textController;
  bool _isTextEditing = false;
  final Set<Polyline> _polyLines = {};
  Set<Polyline> get polyLines => _polyLines;
  static LatLng latLng;
  static LatLng _dragedPosition;


  static const LatLng _center = const LatLng(45.521563, -122.677433);
  final Set<Marker> _markers = {};
  final Set<Polyline> _polylines = {};

  LatLng _lastMapPosition = _center;

  List<LatLng> latlng = [
    LatLng(45.521563, -122.677433),
    LatLng(45.341563, -112.677433),
  ];

  void _onAddMarkerButtonPressed() {
    setState(() {
      _markers.add(Marker(
        // This marker id can be anything that uniquely identifies each marker.
        markerId: MarkerId(_lastMapPosition.toString()),
        position: _lastMapPosition,
        infoWindow: InfoWindow(
          title: 'Custom Marker',
          snippet: 'Inducesmile.com',
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));
      _polylines.add(Polyline(
        polylineId: PolylineId(_lastMapPosition.toString()),
        visible: true,
        points: latlng,
        color: Colors.red,
      ));
    });
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    focusNode = FocusNode();
    _textController = TextEditingController(text: _address);
    _textController.addListener(() {
      setState(() {

      });
    });
    timer = Timer.periodic(Duration(seconds: 3), (timer) {
      _getCurrentLocation();
      _setMarkerIcon();
    });

  }

  @override
  void dispose() {
    // TODO: implement dispose
//    focusNode.dispose();
    super.dispose();
    timer.cancel();
    _textController.dispose();
  }


  void _setMarkerIcon() async {
    _markerIcon = await BitmapDescriptor.fromAssetImage(ImageConfiguration(), 'images/car.png');

  }

  getPoints() {
    return [
      LatLng(6.862472, 79.859482),
      LatLng(6.862258, 79.862325),
      LatLng(6.863121, 79.863644),
      LatLng(6.864538, 79.865039),
      LatLng(6.865124, 79.864546),
      LatLng(6.866451, 79.864667),
      LatLng(6.867303, 79.86544),
      LatLng(6.867899, 79.865826),
      LatLng(6.867867, 79.866727),
      LatLng(6.864884, 79.870333),
      LatLng(6.861859, 79.873112),
      LatLng(6.861593, 79.87499),
      LatLng(6.860837, 79.876427),

    ];
  }
//  void _onMapCreated(GoogleMapController controller){
//    _mapController = controller;
//
//    setState(() {
//      _markers.add(
//        Marker(
//          position: LatLng( _currentPosition.latitude, _currentPosition.longitude),
//          infoWindow: InfoWindow(
//            title: 'hdvfgvf',
//            snippet: 'vdghcvgsvchvf',
//          ),
//          icon: _markerIcon,
//          markerId: MarkerId("0"),
//        ),
//      );
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _currentPosition == null ? Center(child: CircularProgressIndicator(),) : Stack(
        children: [
          GoogleMap(
            mapType: MapType.normal,
            mapToolbarEnabled: false,
            scrollGesturesEnabled: true,
            zoomGesturesEnabled: true,
            zoomControlsEnabled: false,
            trafficEnabled: true,
            compassEnabled: true,
            indoorViewEnabled: true,
            rotateGesturesEnabled: true,
            tiltGesturesEnabled: true,
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            onCameraMove: _onCameraMove,
            polylines: _polyLines,
            onMapCreated: _onMapCreated,
            markers: Set<Marker>.of(
                  <Marker>[
                    Marker(
                      onTap: (){
                        print("Marker clicked");
//                        _modalBottomSheetMenu();
                      },
                      draggable: true,
                      markerId: MarkerId("1"),
                      position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
                      onDragEnd: ((value) {
                        print(value.latitude);
                        print(value.longitude);
                       _dragedPosition= value;
                        // address = _getUserLocation(value.latitude,value.longitude);
                        sendRequest();
                      }),
                      icon: BitmapDescriptor.defaultMarker,
                      infoWindow: const InfoWindow(
                        title: 'Marker',
                      ),
                    )
                  ],
                ),
            initialCameraPosition: CameraPosition(
             target: LatLng(
                 _currentPosition.latitude, _currentPosition.longitude),
             zoom: 14.4746,
           ),
            // polygons: Set<Polygon>.of(<Polygon>[
            //   Polygon(
            //       polygonId: PolygonId('area'),
            //       points: getPoints(),
            //       geodesic: true,
            //       strokeColor: Colors.red.withOpacity(0.6),
            //       strokeWidth: 5,
            //       fillColor: Colors.redAccent.withOpacity(0.1),
            //       visible: true),


            // ]),
            ),
//          GoogleMap(
//            mapType: MapType.normal,
//            mapToolbarEnabled: false,
//            scrollGesturesEnabled: true,
//            zoomGesturesEnabled: true,
//            zoomControlsEnabled: false,
//            trafficEnabled: true,
//            compassEnabled: true,
//            indoorViewEnabled: true,
//            rotateGesturesEnabled: true,
//            tiltGesturesEnabled: true,
//            myLocationButtonEnabled: true,
//            myLocationEnabled: true,
//            initialCameraPosition: CameraPosition(target: _center, zoom: 10.0),
////            CameraPosition(
////              target: LatLng(
////                  _currentPosition.latitude, _currentPosition.longitude),
////              zoom: 14.4746,
////            ),
//            onMapCreated: _onMapCreated,
//            polylines: _polyLines,
//            markers: _markers,
//            onCameraMove: _onCameraMove,
//          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.topRight,
              child: FloatingActionButton(
                onPressed: _onAddMarkerButtonPressed,
                materialTapTargetSize: MaterialTapTargetSize.padded,
                backgroundColor: Colors.green,
                child: const Icon(Icons.add_location, size: 36.0),
              ),
            ),
          ),
//          Container(
//            alignment: Alignment.bottomCenter,
//            padding: EdgeInsets.fromLTRB(0, 0, 0, 32),
//            child: Text('Transport project'),
//          ),
//          Container(
//            alignment: Alignment.bottomCenter,
//            child: Padding(
//              padding: const EdgeInsets.fromLTRB(30, 20, 30, 50),
//              child: Container(
//                height: 100,
//                alignment: Alignment.bottomCenter,
//                color: Colors.white,
//                padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
//                child: Column(
//                  children: [
//                    Row(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: [
//                        Icon(
//                          Icons.my_location,
//                          color: Colors.cyan,
//                          size: 24,
//                        ),
//                        SizedBox(width: 20,),
//                        Column(
//                          children: [
//                            Container(
//                              width: 250,
//                              height: 30,
//                              child: TextField(
////                                focusNode: focusNode,
//                                controller: _textController,
//                                decoration: InputDecoration(
//                                  border: InputBorder.none,
//                                  hintText: 'Meet me at ...',
//                                ),
//                              ),
//                            ),
//                          ],
//                        ),
//                      ],
//
//                    ),
//                    const Divider(
//                      color: Colors.grey,
//                      indent: 35,
//                      thickness: 1,
//                    ),
//                    Row(
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: [
//                        Icon(
//                          Icons.pin_drop,
//                          color: Colors.deepOrangeAccent,
//                          size: 25,
//                        ),
//                        SizedBox(width: 20,),
//                        Column(
//                          children: [
//                            Padding(
//                              padding: const EdgeInsets.only(top: 5),
//                              child: GestureDetector(
//                                onTap: (){
//                                  print('Clicked');
//                                  Navigator.push(context,
//                                      MaterialPageRoute(builder: (context) => (PinMapScreen(currentLat: _currentPosition.latitude, currentLong: _currentPosition.longitude,))));
//                                },
//                                child: Container(
//                                  color: Colors.transparent,
//                                  width: 250,
//                                  height: 30,
//                                  child: IgnorePointer(
//                                    child: Text('Where ? ',
////                                  focusNode: focusNode,
//                                      decoration: InputDecoration(
//                                          border: InputBorder.none,
//                                          hintText: 'I am going to ....',
//                                          hintStyle: TextStyle(
//                                            color: Colors.grey,
//                                            fontSize: 16,
//                                            fontWeight: FontWeight.w700,
//                                          )
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            ),
//                          ],
//                        ),
//                      ],
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),

        ],
      ),
    );
  }


  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        _getAddressFromLatLng(position);
        latLng =  LatLng(position.latitude, position.longitude);
      });
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng(position) async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      String name = place.name;
      String subLocality = place.subLocality;
      String locality = place.locality;
      String administrativeArea = place.administrativeArea;
      String postalCode = place.postalCode;
      String country = place.country;
      String address = "${name}, ${subLocality}, ${locality}, ${administrativeArea} ${postalCode}, ${country}";
      setState(() {
        _address = address;
        print(address);

      });
    } catch (e) {
      print(e);
    }
  }

//  void _onAddMarkerButtonPressed() {
//    setState(() {
//      _markers.add(Marker(
//        markerId: MarkerId("111"),
//        position: latLng,
//        icon: BitmapDescriptor.defaultMarker,
//      ));
//    });
//  }

//  Future<String> getRouteCoordinates(LatLng l1, LatLng l2)async{
//    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=${l1.latitude},${l1.longitude}&destination=${l2.latitude},${l2.longitude}&key=$apiKey";
//    http.Response response = await http.get(url);
//    Map values = jsonDecode(response.body);
//    return values["routes"][0]["overview_polyline"]["points"];
//  }
  void createRoute(String encondedPoly) {
    _polyLines.add(Polyline(
        polylineId: PolylineId(latLng.toString()),
        width: 4,
        points: _convertToLatLng(_decodePoly(encondedPoly)),        color: Colors.red));
  }

  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }

  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;
      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      }
      while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);
    for (var i = 2; i < lList.length; i++)
      lList[i] += lList[i - 2];
    print(lList.toString());
    return lList;
  }
//
//  void sendRequest() async {
//    LatLng destination = LatLng(20.008751, 73.780037);
//    String route = await getRouteCoordinates(latLng, destination);
//    createRoute(route);
//    _addMarker(destination,"KTHM Collage");
//  }

  Future<String> getRouteCoordinates(LatLng l1, LatLng l2)async{
      String url = "https://maps.googleapis.com/maps/api/directions/json?origin=${l1.latitude},${l1.longitude}&destination=${l2.latitude},${l2.longitude}&key=AIzaSyBH6v3Nd0wJh0d5ybcWbpJnXy0CRD-7Xis";
      http.Response response = await http.get(url);
      Map values = jsonDecode(response.body);
      print("-----> values: $values");
      String point = values["routes"][0]["overview_polyline"]["points"];
      print("-----> Point: $point");
      return values["routes"][0]["overview_polyline"]["points"];
  }
  void sendRequest() async {
    String route = await getRouteCoordinates(
        LatLng(_currentPosition.latitude, _currentPosition.longitude), _dragedPosition);
    createRoute(route);
  }

}
