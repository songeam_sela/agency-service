import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:tansport_service_api/Model/route_location.dart';


class ConfirmPickUp extends StatefulWidget {

//  double currentLat, currentLong;
 String address;
 RouteLocation userRoute;


 ConfirmPickUp ({Key key , this.userRoute, this.address}) : super(key: key);
  @override
  _ConfirmPickUpState createState() => _ConfirmPickUpState();
}

class _ConfirmPickUpState extends State<ConfirmPickUp> {
  String address = "";
  Set<Marker> _markers = {};
  Completer<GoogleMapController> _controller = Completer();
  Timer timer;
  Position _currentPosition;
  String placeId;
  LatLng start;
  LatLng end;

  final Set<Polyline> _polyLines = {};
  Set<Polyline> get polyLines => _polyLines;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    start = LatLng(widget.userRoute.startLocation.lat, widget.userRoute.startLocation.long);
    end = LatLng(widget.userRoute.endLocation.lat, widget.userRoute.endLocation.long);
    sendRequest();
    // timer = Timer.periodic(Duration(seconds: 5), (timer) {
    //   // _getCurrentLocation();
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _currentPosition == null ? Center(child: CircularProgressIndicator(),) : Stack(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height - 190,
            child: GoogleMap(
              mapType: MapType.normal,
              mapToolbarEnabled: false,
              scrollGesturesEnabled: true,
              zoomGesturesEnabled: true,
              zoomControlsEnabled: false,
              trafficEnabled: true,
              compassEnabled: true,
              indoorViewEnabled: true,
              rotateGesturesEnabled: true,
              tiltGesturesEnabled: true,
              myLocationButtonEnabled: true,
              myLocationEnabled: true,
              markers: Set<Marker>.of(
                  <Marker>[
                    Marker(
                      onTap: (){
                        print("Marker clicked");
//                        _modalBottomSheetMenu();
                      },
                      draggable: true,
                      markerId: MarkerId("1"),
                      position: LatLng(widget.userRoute.endLocation.lat, widget.userRoute.endLocation.long),
                      // onDragEnd: ((value) {
                      //   print(value.latitude);
                      //   print(value.longitude);
                      //  _dragedPosition= value;
                      //   // address = _getUserLocation(value.latitude,value.longitude);
                      //   sendRequest();
                      // }),
                      icon: BitmapDescriptor.defaultMarker,
                      infoWindow: const InfoWindow(
                        title: 'Marker',
                      ),
                    )
                  ],
                ),
            initialCameraPosition: CameraPosition(
             target: LatLng(
                 widget.userRoute.startLocation.lat, widget.userRoute.startLocation.long),
             zoom: 14.4746,
           ),
              // initialCameraPosition: CameraPosition(
              //     target: LatLng(_currentPosition.latitude, _currentPosition.longitude),
              //     zoom: 15
              // ),
            //   markers: Set<Marker>.of(
            //     <Marker>[
            //       Marker(
            //         onTap: (){
            //           print("Marker clicked");
            //         },
            //         draggable: true,
            //         markerId: MarkerId("1"),
            //         position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
            //         onDragEnd: ((value) {
            //           print(value.latitude);
            //           print(value.longitude);
            //           address = _getUserLocation(value.latitude,value.longitude);
            //         }),
            //         icon: BitmapDescriptor.defaultMarker,
            //         infoWindow: const InfoWindow(
            //           title: 'Marker',
            //         ),
            //       )
            //     ],
            //   ),
            //   onMapCreated: (GoogleMapController controller) {
            //     _controller.complete(controller);
            //   },
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 200,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.0),
                    topLeft: Radius.circular(10.0),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                    margin: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.grey.shade200,
                    ),
                    child: Row(
                      children: [
                        Icon(Icons.my_location, color: Colors.cyan,size: 20,),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: Container(
                            width: MediaQuery.of(context).size.width - 120,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Institute of Technology of Cambodia',
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                ),),
                                Text('${address}',
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                style: TextStyle(
                                  fontWeight: FontWeight.w200,
                                  fontSize: 12,
                                ),),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: FlatButton(
                      onPressed: (){},
                      child: Text('Confirm Pick Up',
                      style: TextStyle(
                        color: Colors.white
                      ),),
                      color: Colors.blue,
                      padding: EdgeInsets.only(top: 12, bottom: 12,
                          left:  110,
                          right:  110),
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0))
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void createRoute(String encondedPoly) {
    LatLng latLng = LatLng(widget.userRoute.startLocation.lat, widget.userRoute.startLocation.long);
    _polyLines.add(Polyline(
        polylineId: PolylineId(latLng.toString()),
        width: 4,
        points: _convertToLatLng(_decodePoly(encondedPoly)),        color: Colors.red));
  }

  List<LatLng> _convertToLatLng(List points) {
    List<LatLng> result = <LatLng>[];
    for (int i = 0; i < points.length; i++) {
      if (i % 2 != 0) {
        result.add(LatLng(points[i - 1], points[i]));
      }
    }
    return result;
  }

  List _decodePoly(String poly) {
    var list = poly.codeUnits;
    var lList = new List();
    int index = 0;
    int len = poly.length;
    int c = 0;
    do {
      var shift = 0;
      int result = 0;
      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      }
      while (c >= 32);
      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);
    for (var i = 2; i < lList.length; i++)
      lList[i] += lList[i - 2];
    print(lList.toString());
    return lList;
  }
//
//  void sendRequest() async {
//    LatLng destination = LatLng(20.008751, 73.780037);
//    String route = await getRouteCoordinates(latLng, destination);
//    createRoute(route);
//    _addMarker(destination,"KTHM Collage");
//  }

  Future<String> getRouteCoordinates(LatLng l1, LatLng l2)async{
      String url = "https://maps.googleapis.com/maps/api/directions/json?origin=${l1.latitude},${l1.longitude}&destination=${l2.latitude},${l2.longitude}&key=AIzaSyBH6v3Nd0wJh0d5ybcWbpJnXy0CRD-7Xis";
      http.Response response = await http.get(url);
      Map values = jsonDecode(response.body);
      print("-----> values: $values");
      String point = values["routes"][0]["overview_polyline"]["points"];
      print("-----> Point: $point");
      return values["routes"][0]["overview_polyline"]["points"];
  }
  void sendRequest() async {
    String route = await getRouteCoordinates(
        start, end);
    createRoute(route);
  }

  _getUserLocation(double lat, double long) async {//call this async method from whereever you need

    final coordinates = new Coordinates(
        lat, long);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;
    print(' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
    var add = ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}';
    return add;
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() async {
        _currentPosition = position;
        placeId = await getPlaceId();
        print("-----> placeid2: ${placeId}");
      });
    }).catchError((e) {
      print(e);
    });
  }

  Future<String> getPlaceId() async {
    String url =
        "https://maps.googleapis.com/maps/api/geocode/json?&latlng=${_currentPosition.latitude},${_currentPosition.longitude}&&key=AIzaSyBH6v3Nd0wJh0d5ybcWbpJnXy0CRD-7Xis";
    http.Response response = await http.get(url);
    var values = jsonDecode(response.body);
    print("-----> values: $values");
    String placeid = values["results"][0]["place_id"];
    print("-----> placeId: $placeid");  
    setState(() {
      placeId = values["results"][0]["place_id"];
    });  
    return placeid;
  }


}

