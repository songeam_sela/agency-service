import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:google_maps_place_picker/google_maps_place_picker.dart';

import 'confirm_pickup.dart';

class PinMapScreen extends StatefulWidget {
  double currentLat, currentLong;

  PinMapScreen({Key key, this.currentLat, this.currentLong}) : super(key: key);
  @override
  _PinMapScreenState createState() => _PinMapScreenState();
}

class _PinMapScreenState extends State<PinMapScreen> {
//  PickResult selectedPlace;
  String address = "";
  LatLng pinLatLong;
  GoogleMapController googleMapController;
  final Geolocator _geolocator = Geolocator()..forceAndroidLocationManager;
  Position _position;
  String _address = "";
  

  String latitude = "";
  String longitude = "";

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width *
        MediaQuery.of(context).devicePixelRatio;
    double screenHeight = MediaQuery.of(context).size.height *
        MediaQuery.of(context).devicePixelRatio;

    double middleX = screenWidth / 2;
    double middleY = screenHeight / 2;

    ScreenCoordinate screenCoordinate =
        ScreenCoordinate(x: middleX.round(), y: middleY.round());

    Set<Marker> _markers = {};
    Completer<GoogleMapController> _controller = Completer();
    return Scaffold(
      body: SizedBox(
        child: Stack(
          children: [
            GoogleMap(
              mapType: MapType.normal,
              mapToolbarEnabled: false,
              scrollGesturesEnabled: true,
              zoomGesturesEnabled: true,
              zoomControlsEnabled: false,
              trafficEnabled: true,
              compassEnabled: true,
              indoorViewEnabled: true,
              rotateGesturesEnabled: true,
              tiltGesturesEnabled: true,
              myLocationButtonEnabled: false,
              myLocationEnabled: true,
              initialCameraPosition: CameraPosition(
                  target: LatLng(widget.currentLat, widget.currentLong),
                  zoom: 15),
//               markers: Set<Marker>.of(
//                 <Marker>[
//                   Marker(
//                     onTap: () {
//                       print("Marker clicked");
// //                        _modalBottomSheetMenu();
//                     },
//                     draggable: true,
//                     markerId: MarkerId("1"),
//                     position: LatLng(widget.currentLat, widget.currentLong),
//                     onDragEnd: ((value) async {
//                       print(value.latitude);
//                       print(value.longitude);
//                       // address = _getUserLocation(value.latitude,value.longitude);
//                       // print ("-----> 1: "+ address);
//                       // address = await getAddress(value.latitude, value.longitude);
//                       // getAddress(value.latitude, value.longitude);
//                       // print ("-----> 2: "+ address);
//                     }),
//                     icon: BitmapDescriptor.defaultMarker,
//                     infoWindow: const InfoWindow(
//                       title: 'Marker',
//                     ),
//                   )
//                 ],
//               ),
              onMapCreated: (GoogleMapController controller) async {
                _controller.complete(controller);
                googleMapController = controller;
              },
              onCameraIdle: () async {
                pinLatLong =
                    await googleMapController.getLatLng(screenCoordinate);
                address =
                    await getAddress(pinLatLong.latitude, pinLatLong.longitude);
                print("------> $pinLatLong");
                print("-----> 2: " + address);
                // _fetchLocation();
              },
            ),
            Align(
              alignment: Alignment.center,
              child: Icon(Icons.place, size: 35, color: Colors.red,),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 40, left: 20),
              child: Align(
                alignment: Alignment.topLeft,
                child: Container(
                  alignment: Alignment.center,
//                    padding: EdgeInsets.only(left: 3, bottom: 3),
                  width: 36,
                  height: 36,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(18.0),
                      color: Colors.white),
                  child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.arrow_back_ios,
                      size: 23,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, bottom: 25),
                child: GestureDetector(
                  onTap: () => Navigator.push(context,  MaterialPageRoute(builder: (context) => (ConfirmPickUp()))),
                    child: Container(
                    width: MediaQuery.of(context).size.width - 40,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.cyan,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Confirm',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _getUserLocation(double lat, double long) async {
    //call this async method from whereever you need

    final coordinates = new Coordinates(lat, long);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    print(
        ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
    String add =
        ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}';

    return add;
  }

  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() async {
        _position = position;
        _getPlace();
        _address = await getAddress(position.latitude, position.longitude);
      });
    }).catchError((e) {
      print(e);
    });
  }

  void _getPlace() async {
    List<Placemark> placemark = await _geolocator.placemarkFromCoordinates(
        _position.latitude, _position.longitude);

    // this is all you need
    Placemark placeMark = placemark[0];
    String name = placeMark.name;
    String subLocality = placeMark.subLocality;
    String locality = placeMark.locality;
    String administrativeArea = placeMark.administrativeArea;
    String postalCode = placeMark.postalCode;
    String country = placeMark.country;
    String address =
        "${name}, ${subLocality}, ${locality}, ${administrativeArea} ${postalCode}, ${country}";

    print("----->>" + address);

    print(placemark[0].country);
    print(placemark[0].position);
    print(placemark[0].locality);
    print(placemark[0].administrativeArea);
    print(placemark[0].postalCode);
    print(placemark[0].name);
    // print(placemark[0].subAdministratieArea);
    print(placemark[0].isoCountryCode);
    print(placemark[0].subLocality);
    print(placemark[0].subThoroughfare);
    print(placemark[0].thoroughfare);

    setState(() {
      _address = address; // update _address
    });
  }

//  void _modalBottomSheetMenu(){
//    showModalBottomSheet(
//      isDismissible: false,
//        context: context,
//        builder: (builder){
//          return new Container(
//            height: 350.0,
//            color: Colors.transparent, //could change this to Color(0xFF737373),
//            //so you don't have to change MaterialApp canvasColor
//            child: new Container(
//                decoration: new BoxDecoration(
//                    color: Colors.white,
//                    borderRadius: new BorderRadius.only(
//                        topLeft: const Radius.circular(10.0),
//                        topRight: const Radius.circular(10.0))),
//                child: new Center(
//                  child: new Text("This is a modal sheet"),
//                )),
//          );
//        }
//    );
//  }

  _fetchLocation() async {
    Position position = await _geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);

    ///Here you have choose level of distance
    latitude = position.latitude.toString() ?? '';
    longitude = position.longitude.toString() ?? '';
    List<Placemark> placemark = await _geolocator.placemarkFromCoordinates(
        position.latitude, position.longitude);
    address =
        '${placemark.first.name.isNotEmpty ? placemark.first.name + ', ' : ''}${placemark.first.thoroughfare.isNotEmpty ? placemark.first.thoroughfare + ', ' : ''}${placemark.first.subLocality.isNotEmpty ? placemark.first.subLocality + ', ' : ''}${placemark.first.locality.isNotEmpty ? placemark.first.locality + ', ' : ''}${placemark.first.subAdministrativeArea.isNotEmpty ? placemark.first.subAdministrativeArea + ', ' : ''}${placemark.first.postalCode.isNotEmpty ? placemark.first.postalCode + ', ' : ''}${placemark.first.administrativeArea.isNotEmpty ? placemark.first.administrativeArea : ''}';
    print("latitude" + latitude);
    print("longitude" + longitude);
    print("adreess" + address);

    print(placemark[0].country);
    print(placemark[0].position);
    print(placemark[0].locality);
    print(placemark[0].administrativeArea);
    print(placemark[0].postalCode);
    print(placemark[0].name);
    // print(placemark[0].subAdministratieArea);
    print(placemark[0].isoCountryCode);
    print(placemark[0].subLocality);
    print(placemark[0].subThoroughfare);
    print(placemark[0].thoroughfare);
  }

  Future<String> getAddress(double l1, double l2) async {
    String url =
        "https://maps.googleapis.com/maps/api/geocode/json?&latlng=${l1},${l2}&&key=AIzaSyBH6v3Nd0wJh0d5ybcWbpJnXy0CRD-7Xis";
    http.Response response = await http.get(url);
    var values = jsonDecode(response.body);
    print("-----> values: $values");
    String address = values["results"][0]["formatted_address"];
    print("-----> address: $address");

    setState(() {
      _address = address;
      // placeId = values["results"][0]["place_id"];
    });
    return address;
  }
}
