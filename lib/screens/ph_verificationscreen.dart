import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:tansport_service_api/screens/maphome_screen.dart';
import 'package:tansport_service_api/screens/authenticate/sign_up.dart';
import 'package:tansport_service_api/services/auth.dart';

class PhoneVerificationScreen extends StatefulWidget {
  final phone;
  final smsCode;
//  final autoCode;
//  final smsCode;
//  final credential;
//  PhoneVerificationScreen({this.phone,this.smsCode,this.autoCode,this.credential});
  PhoneVerificationScreen({this.phone,this.smsCode});
  @override
  _PhoneVerificationScreenState createState() =>
      _PhoneVerificationScreenState();
}

class _PhoneVerificationScreenState extends State<PhoneVerificationScreen> {
  final _formKey = GlobalKey<FormState>();
  bool _onEditing = true;
  TextEditingController _codeController ;
//  String _code;
  bool _loading = true;
  @override
  void initState() {
    super.initState();
    _codeController = TextEditingController(text: widget.smsCode);

  }
  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 3),(){
      Navigator.pop(context);
    });
    return Scaffold(
      body: LoadingOverlay(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _Input(),
              ],
            ),
          ),
        ), isLoading: _loading
      ),
    );
  }
  Widget _Input() {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Phone Verification',
              style: TextStyle(
                fontFamily: 'Helvetica Neue',
                fontSize: 30,
                color: const Color(0xf5296af4),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Enter the 6-digits code that sent to '+widget.phone,
              style: TextStyle(
                fontFamily: 'Helvetica Neue',
                fontSize: 14,
                color: const Color(0xff524949),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Form(
            key: _formKey,
            child: TextFormField(
              validator: (val)=> val.length>0?null: 'Phone Number Required',
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                hintText: ' 000000',
                hintStyle: TextStyle(color: Colors.grey.withOpacity(0.3))
              ),
              controller: _codeController,
            )
        ),
        SizedBox(
          height: 30,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 186,
              height: 43,
              child: FlatButton(

                color: const Color(0xf5296af4),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15.0),
                ),
                child: Text(
                  'Continue ',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: 19,
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                  ),
                ),
                onPressed: () {
//                  if(_codeController.text == widget.smsCode){
//                    await AuthService().signInWithCredential(widget.credential);
//                  }
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
