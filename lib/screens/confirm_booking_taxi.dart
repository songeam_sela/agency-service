import 'package:flutter/material.dart';

class ConfirmBookingTaxi extends StatefulWidget {
  @override
  _ConfirmBookingTaxiState createState() => _ConfirmBookingTaxiState();
}

class _ConfirmBookingTaxiState extends State<ConfirmBookingTaxi> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height - 190,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Colors.blue,
              width: MediaQuery.of(context).size.width,
              height: 200,
              child: Column(
                children: [
                  Text('Booking Confirmation'),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Total Price'),
                        Text('Max of People'),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Divider(
                      color: Colors.grey,
                      indent: 0,
                      thickness: 1,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                          child: Text('\$ 45.00')),
                      VerticalDivider(
                        indent: 5,
                        thickness: 10,
                        color: Colors.redAccent,
                      ),
                      Container(child: Text('4 People'))
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
