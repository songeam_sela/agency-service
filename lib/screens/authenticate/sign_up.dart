import 'package:flutter/material.dart';
import 'package:tansport_service_api/screens/maphome_screen.dart';
import 'package:tansport_service_api/services/auth.dart';

class SignUp extends StatefulWidget {
  final Function toggleView;
  SignUp({this.toggleView});
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  bool _isHidden = true;
  final _formKey = GlobalKey<FormState>();
  bool loading = false;
  TextEditingController nameInputController;
  TextEditingController emailInputController;
  TextEditingController pwdInputController;
  String error = '';
  void _togglePasswordVisibility(){
    setState(() {
      _isHidden = !_isHidden;
    });
  }
  @override
  initState() {
    nameInputController = new TextEditingController();
    emailInputController = new TextEditingController();
    pwdInputController = new TextEditingController();
    super.initState();
  }
  @override

  Widget build(BuildContext context) {
    return loading?Center(child: CircularProgressIndicator(),):Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 80, left: 30,right: 30),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _header(),
              SizedBox(height: 20,),
              _form(),
              _footer(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _header(){
    return Row(
      children: <Widget>[
        Text('Sign Up',
          style: TextStyle(
            fontFamily: 'Helvetica Neue',
            fontSize: 30,
            fontWeight: FontWeight.bold,
            color: const Color (0xf5296af4),
          ),),

      ],
    );
  }

  Widget _form(){
    return Column(
      children: [
          Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              _textField("Full Name"),
              SizedBox(height: 20,),
    //        _textField("Phone Number"),
    //        SizedBox(height: 20,),
              _textField("Email"),
              SizedBox(height: 20,),
              _textField("Password"),
              SizedBox(height: 20,),
    //        _textField("Confirm Password"),
    //        SizedBox(height: 20,),
            ]
          )
        ),
        SizedBox(height: 20,),
        RichText(
          text: TextSpan(
              text: 'By signing up you accept the ',
              style: TextStyle(
                fontFamily: 'Helvetica Neue',
                fontSize: 14,
                color: const Color(0xff524949),
              ),
              children: <TextSpan>[
                TextSpan(
                  text: 'Term of service',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: 14,
                    color: const Color (0xf5296af4),
                  ),
                ),
                TextSpan(
                  text: ' and',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: 14,
                    color: const Color(0xff524949),
                  ),
                ),
                TextSpan(
                  text: ' Privacy Policy',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: 14,
                    color: const Color (0xf5296af4),
                  ),
                ),
              ]
          ),
        ),
        ],
        );
  }

  Widget _textField( String hintText){
    return TextFormField(
      validator: (val) =>
      val.isEmpty ? 'error' : null,
      keyboardType: hintText == "Email" ? TextInputType.emailAddress : hintText == "Phone Number" ? TextInputType.phone : TextInputType.text,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: Colors.blue),
        ),
        hintText: hintText,
        hintStyle: TextStyle(fontFamily: 'Helvetica Neue',fontSize: 16,),
        suffixIcon: hintText == "Password" ? IconButton(
          onPressed: _togglePasswordVisibility,
          icon: _isHidden ?  Icon(Icons.visibility_off,) : Icon(Icons.visibility),
        ): null,
      ),
      obscureText: hintText == "Password" ? _isHidden : hintText == "Confirm Password" ? true : false,
      controller: hintText=="Full Name"?nameInputController:hintText=="Email"?emailInputController:pwdInputController,
    );
  }

  Widget _footer(){
    return Column(
      children: <Widget>[
        SizedBox(height: 20,),
        Container(
          width: 303,
          height: 41,
          child: FlatButton(
            color: const Color (0xf5296af4),
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(15.0),
            ),
            child:Text('Sign Up ',
              style: TextStyle(
                fontFamily: 'Helvetica Neue',
                fontSize: 19,
                fontWeight: FontWeight.normal,
                color: Colors.white,
              ),
            ),
            onPressed: () async {
//              if (_formKey.currentState.validate()) {
//                print(emailInputController.text.trim());
//                print(nameInputController.text);
//                print(pwdInputController.text);
//                final _auth = AuthService();
//                setState(() => loading = true);
//                dynamic result = await _auth.registerNewUser(emailInputController.text.trim(),pwdInputController.text,nameInputController.text.trim());
//                if(result==null){
//                  setState(() {
//                    loading = false;
//                    error = 'please input a valid email';
//                  });
//                }
//              }
            },
//            onPressed: () {
//              Navigator.push(context, MaterialPageRoute(builder: (context) => (MapHomeScreen())));
//            },
          ),
        ),
        SizedBox(height: 10,),
        Text(
          error,
          style: TextStyle(color: Colors.red, fontSize: 14),
        ),
        SizedBox(height: 10,),
        GestureDetector(
          onTap: (){
            widget.toggleView();
          },
          child: RichText(
            text: TextSpan(
                text: 'Already have an account? ',
                style: TextStyle(
                  fontFamily: 'Helvetica Neue',
                  fontSize: 14,
                  color: const Color(0xff524949),
                ),
                children: <TextSpan>[
                  TextSpan(text: 'Sign In',
                    style: TextStyle(
                      fontFamily: 'Helvetica Neue',
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      color: const Color (0xf5296af4),
                    ),
                  ),
                ]
            ),
          ),
        ),
      ],
    );
  }
}