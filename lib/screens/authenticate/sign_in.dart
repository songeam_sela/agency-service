import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:tansport_service_api/screens/maphome_screen.dart';
import 'package:tansport_service_api/screens/ph_verificationscreen.dart';
//import 'package:tansport_service_api/screens/ph_verificationscreen.dart';
import 'package:tansport_service_api/services/auth.dart';

class SignIn extends StatefulWidget {
  final Function toggleView;
  SignIn({this.toggleView});
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();
  bool loading = false;
  final _phoneController = TextEditingController();
  String _phoneCode = '';
  Widget _buildDropdownItem(Country country) => Container(
    child: Row(
      children: <Widget>[
        CountryPickerUtils.getDefaultFlagImage(country),
        SizedBox(
          width: 8.0,
        ),
        Text("+${country.phoneCode}(${country.isoCode})"),
      ],
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: LoadingOverlay(
        child: Padding(
          padding: const EdgeInsets.only(left: 20,right: 20),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _header(),
//              SizedBox(height: 100,),
                Expanded(
                  flex: 2,
                  child: Column(
                      children: <Widget>[
                        _formInput(),
//              SizedBox(height: 50,),
                        _footer(),
                      ]
                  ),
                ),
              ],
            ),
          ),
        ),isLoading: loading,
      ),
    );
  }

  Widget _header(){
    return Expanded(
      flex: 1,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center ,
            children: <Widget>[
              Container(
                width: 290,
                height: 100,
                decoration: new BoxDecoration(
                  image: new DecorationImage(image: new AssetImage("images/Image_icon.png")),
                ),
                child: Center(
                  child: Text('Agency Services', style: TextStyle(
                      fontFamily: 'SF Pro Text',
                      fontSize: 25,
                      color: Colors.blue,
                      shadows: <Shadow>[
                        Shadow(
                          offset: Offset(2.0, 2.0),
                          blurRadius: 1.0,
                          color: Color.fromARGB(145, 145, 145, 145),
                        )
                      ]
                  ),),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _formInput(){
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text('Welcome',
              style: TextStyle(
                fontFamily: 'Helvetica Neue',
                fontSize: 40,
                color: const Color (0xf5296af4),
              ),
            ),
          ],
        ),
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text('Enter your mobile number to continue',
              style: TextStyle(
                fontFamily: 'Helvetica Neue',
                fontSize: 14,
                color: const Color(0xff524949),
              ),
            ),
          ],
        ),
        SizedBox(height: 20,),
//
        Row(
          children: [
            CountryPickerDropdown(
              initialValue: 'JP',
              itemBuilder: _buildDropdownItem,
              priorityList:[
                CountryPickerUtils.getCountryByIsoCode('GB'),
                CountryPickerUtils.getCountryByIsoCode('CN'),
              ],
              sortComparator: (Country a, Country b) => a.isoCode.compareTo(b.isoCode),
              onValuePicked: (Country country) {
                _phoneCode = '+'+country.phoneCode;
              },
            ),
            Expanded(
              child: Form(
                key: _formKey,
                child: _phone(),
              ),
            )
          ],
        ),
        SizedBox(height: 30,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 186,
              height: 43,
              child: FlatButton(
                color: const Color (0xf5296af4),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15.0),
                ),
                child:Text('Continue ',
                  style: TextStyle(
                    fontFamily: 'Helvetica Neue',
                    fontSize: 19,
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                  ),
                ),
                onPressed: () async{

                  final phone = _phoneCode+_phoneController.text.trim();
                  if (_formKey.currentState.validate()) {
                    setState(() {
                      loading= true;
                    });
                    final _auth = AuthService();
                        await _auth.signInWithPhoneNumber(phone,context);
//                    }
                  }
                },
              ),
            ),
          ],
        ),
      ],
    );

  }

  Widget _phone(){
    return TextFormField(
      validator: (val)=> val.length>0?null: 'Phone Number Required',
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        hintText: ' Phone Number',
      ),
      controller: _phoneController,
    );
  }

  Widget _footer(){
    return Column(
      children: <Widget>[
        SizedBox(height: 20,),
        Text('Or connect with ',
          style: TextStyle(
            fontFamily: 'Helvetica Neue',
            fontSize: 14,
            color: const Color(0xff524949),
          ),
        ),
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 50,
              width: 50,
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("images/facebook.png")),
              ),
            ),
            SizedBox(width: 15,),
            Container(
              height: 50,
              width: 50,
              decoration: new BoxDecoration(
                image: new DecorationImage(image: new AssetImage("images/google.png")),
              ),
            ),
          ],
        ),
        SizedBox(height: 20,),
        GestureDetector(
          onTap: (){
            widget.toggleView();
          },
          child: RichText(
            text: TextSpan(
                text: 'Dont have an account? ',
                style: TextStyle(
                  fontFamily: 'Helvetica Neue',
                  fontSize: 14,
                  color: const Color(0xff524949),
                ),
                children: <TextSpan>[
                  TextSpan(text: 'Sign up',
                    style: TextStyle(
                      fontFamily: 'Helvetica Neue',
                      fontSize: 14,
                      color: const Color (0xf5296af4),
                    ),
                  ),
                ]
            ),
          ),
        ),
      ],
    );

  }

}