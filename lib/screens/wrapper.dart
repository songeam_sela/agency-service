import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:tansport_service_api/Model/my_user.dart';
import 'package:tansport_service_api/screens/maphome_screen.dart';
import 'package:tansport_service_api/screens/ph_verificationscreen.dart';
import 'package:tansport_service_api/services/auth.dart';
import 'authenticate/authenticate.dart';

class Wrapper extends StatefulWidget {
  @override
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<MyUser>(context);
    if(user == null) {
      return Authenticate();
    } else {
//      AuthService().signOut();
      return MapHomeScreen();

    }
  }
}
