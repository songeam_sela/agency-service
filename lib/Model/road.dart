import 'package:json_annotation/json_annotation.dart';
import 'location.dart';
part 'road.g.dart';
@JsonSerializable(fieldRename: FieldRename.snake)

class Road {
  final Location location;
  final int originalIndex;
  final String placeId;

  Road(this.location, this.originalIndex, this.placeId);

  factory Road.fromJson(Map<String, dynamic> json) => _$RoadFromJson(json);

  Map<String, dynamic> toJson() => _$RoadToJson(this);

}



