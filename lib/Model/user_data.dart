class UserData {

  final String id;
  final String fullName;
  final String email;

  UserData({this.id, this.fullName, this.email});
}