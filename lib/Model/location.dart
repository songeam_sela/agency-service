
import 'package:json_annotation/json_annotation.dart';
part 'location.g.dart';
@JsonSerializable(fieldRename: FieldRename.snake)

class Location{
  final double latitude;
  final double longitude;

  Location(this.latitude, this.longitude);

  factory Location.fromJson(Map<String, dynamic> json) => _$LocationFromJson(json);

  Map<String, dynamic> toJson() => _$LocationToJson(this);

}