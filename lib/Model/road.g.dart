// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'road.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Road _$RoadFromJson(Map<String, dynamic> json) {
  return Road(
    json['location'] == null
        ? null
        : Location.fromJson(json['location'] as Map<String, dynamic>),
    json['original_index'] as int,
    json['place_id'] as String,
  );
}

Map<String, dynamic> _$RoadToJson(Road instance) => <String, dynamic>{
      'location': instance.location,
      'original_index': instance.originalIndex,
      'place_id': instance.placeId,
    };
