class RouteLocation {
  String name;
  String uid;
  StartLocation startLocation;
  EndLocation endLocation;

  // RouteLocation(this.name, this.uid, this.startLocation, this.endLocation);

}

class StartLocation {
  double lat;
  double long;

  // StartLocation(this.lat, this.long);
}

class EndLocation {
  double lat;
  double long;

  // EndLocation(this.lat, this.long);
}