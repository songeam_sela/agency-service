import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:tansport_service_api/screens/ph_verificationscreen.dart';
import 'package:tansport_service_api/screens/ph_verificationscreen_manually.dart';
import 'package:tansport_service_api/services/database.dart';
//import 'package:firebase_core/firebase_core.dart';
import '../Model/my_user.dart';
class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  // create user obj based on FirebaseUser
  MyUser _userFromFirebaseUser(User user) {
    return user != null ? MyUser(id: user.uid) : null;
  }

  Future<MyUser> getCurrentUser() async {
    User user = _auth.currentUser;
    return _userFromFirebaseUser(user);
  }

  // auth change user stream
  Stream<MyUser> get user {
    return _auth.authStateChanges()
        .map(_userFromFirebaseUser);
  }

  Future signInWithPhoneNumber(String phone, BuildContext context) async{
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: phone,
      verificationCompleted: (PhoneAuthCredential credential) async{
          Navigator.push(context, MaterialPageRoute(builder: (context) => (PhoneVerificationScreen(phone: phone,smsCode: credential.smsCode))));
         await _auth.signInWithCredential(credential);
      },
      verificationFailed: (FirebaseAuthException e) {},
      codeSent: (String verificationId, int resendToken) {
        Navigator.push(context, MaterialPageRoute(builder: (context) => (PhoneVerificationScreenManually(phone: phone))));
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }


  // sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch(e) {
      print(e.toString());
      return null;
    }
  }

}
