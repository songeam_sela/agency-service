import 'package:cloud_firestore/cloud_firestore.dart';
class DatabaseService{
  final String uid;
  DatabaseService({this.uid});
  final CollectionReference usersDB = FirebaseFirestore.instance.collection('users');

  Future<void> createUserData(String email, String name) async{
    try{
      await usersDB.doc(uid).set({
        'email': email,
        'name': name,
        'created_at': FieldValue.serverTimestamp(),
        'updated_at': FieldValue.serverTimestamp(),

      });

    }catch (err) {
      print('network error: $err');
    }
  }
}