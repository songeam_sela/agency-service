import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tansport_service_api/screens/maphome_screen.dart';
import 'package:tansport_service_api/screens/mapscreen.dart';
import 'package:tansport_service_api/screens/splashscreen.dart';
import 'package:tansport_service_api/services/auth.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider.value(
      value: AuthService().user,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MapHomeScreen(),
      ),
    );
  }
}

