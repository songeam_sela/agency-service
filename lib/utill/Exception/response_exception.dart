import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:i18n_extension/i18n_widget.dart';
class ResponseException implements Exception {
  final http.Response response;

  String getMessage(BuildContext context) {
    Map<String, dynamic> bodyJson = jsonDecode(response.body);
    if (bodyJson.containsKey('message')) {
      if (bodyJson['message'] is Map) {
        if(isEnglish(context)){
          return bodyJson['message']['message_en'];
        }else {return bodyJson['message']['message_km'];}

      }
      return bodyJson['message'].toString();
    } else {
      return "Error[${response.statusCode}]: ${response.reasonPhrase}";
    }
  }

  const ResponseException(this.response);
}

bool isEnglish(BuildContext context) {
  return I18n.of(context).locale?.languageCode == 'en';
}

