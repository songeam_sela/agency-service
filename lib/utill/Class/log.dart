class Log {
  static debug(dynamic value) {
    print('[Debug]: ${value.runtimeType} : $value');
  }

  static error(dynamic value) {
    print("[Error ${value.runtimeType}]:??????");
    if (value is Error) {
      print(value.stackTrace);
    } else if (value is Exception) {
      print(value);
    } else {
      print('$value');
    }
  }

  static info(String value) {
    print('[Info]: $value');
  }
}